# High-On-Helium #



### What is High-On-Helium? ###

High-On-Helium is an application that uses the Helium Developer Kit and network console to gather IoT sensor data.

The Helium Developer Kit consists of two hardware parts:  
the main development board with a LoRa (Long Range) capable radio and an expansion board with six environmental 
& motion MEMS sensors.

LoRa is a long range WAN technology associated with low power consumption and IoT-oriented devices management. 

The development board is programmed with firmware, added to the Helium network using the Helium Console, 
and sensor data is routed to an application to view.